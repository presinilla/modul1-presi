package com.faitechno;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

public class MainActivity extends AppCompatActivity {

    TextView hasil;
    Button cek;
    EditText alas, tinggi;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        alas = findViewById(R.id.ETAlas);
        tinggi = findViewById(R.id.ETTinggi);

        cek = findViewById(R.id.cekButton);
        hasil = findViewById(R.id.hasilTEXT);

        cek.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                hasil.setText(String.valueOf(Integer.parseInt(alas.getText().toString()) * Integer.parseInt(tinggi.getText().toString())));
            }
        });
    }
}
